from flask import Flask, request, jsonify
import datetime, random


app = Flask(__name__)

# Diccionario con preguntas
asignaturas_por_dia = {
    "lunes": "Modelos de Tecnologías para los Sistemas de Información",
    "martes": "Fundamentos del Comercio Electrónico",
    "miércoles": "Gestión y Desarrollo del Talento",
    "jueves": "Gestión de Proyectos ",
    "viernes": "Desarrollo con Tecnologias Emergentes",
}

preguntas = ["Numero secreto", "Dime la hora"]


def calcular_numero_secreto():
    # Obtenemos la hora local del servidor
    hora_actual = datetime.datetime.now().time()

    # Utilizamos algún algoritmo para calcular el número secreto
    # En este caso, simplemente multiplicamos la hora actual por un valor arbitrario
    #numero_secreto = hora_actual.hour * 100 + hora_actual.minute
    numero_secreto = hora_actual.hour * 100 + random.randint(1,100)

    return numero_secreto


def obtener_hora_actual():
    # Obtener la hora actual
    hora_actual = datetime.datetime.now().time()
    hora_formateada = hora_actual.strftime("%H:%M")
    return hora_formateada



@app.route('/')
def home():
    return 'Secret Number API'



@app.route('/pregunta', methods=['POST'])
def pregunta():
    # Obtener la pregunta del cuerpo de la solicitud JSON
    pregunta = request.json['queryResult']['queryText']

    # Inicializar las variables dia y accion
    dia = None
    accion = None

    # Buscar la presencia de un día en la pregunta
    for key in asignaturas_por_dia.keys():
        if key in pregunta.lower():
            dia = key
            break

    # Buscar la presencia de una acción en la pregunta
    for key in preguntas:
        if key.lower() in pregunta.lower():
            accion = key
            break
    
    # Manejar la acción correspondiente
    if accion == "Numero secreto":
        numero_secreto = calcular_numero_secreto()
        respuesta = {
            "fulfillmentText": "El número secreto es {}".format(numero_secreto)
        }
        return jsonify(respuesta)

    elif accion == "Dime la hora":
        hora = obtener_hora_actual()
        respuesta = {
            "fulfillmentText": "Son las {}".format(hora)
        }
        return jsonify(respuesta)

    # Buscar la asignatura correspondiente al día
    elif dia:
        asignatura = asignaturas_por_dia[dia]
        respuesta = f"El día {dia.capitalize()} tienes la asignatura de {asignatura}."
    else:
        respuesta = "Lo siento, no entendí la pregunta o el día no está en mi lista."

    # Crear la respuesta JSON
    respuesta_json = {"fulfillmentText": respuesta}

    return jsonify(respuesta_json)




if __name__ == '__main__':
    app.run()
